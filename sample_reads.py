#!/usr/bin/env python3
#
# Copyright 2022, Julian Catchen <jcatchen@illinois.edu>
#
# sample_reads.py is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# sample_reads.py is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with sample_reads.py.  If not, see <http://www.gnu.org/licenses/>.
#

import argparse
import sys
import os
import gzip
import random

read_fraction  = 0.0
depth_coverage = 0.0
genome_size    = 0
min_len_lim    = 0
max_len_lim    = sys.maxsize
user_seed      = 0

paired   = False
ftype    = "fasta"
se_path  = ""
pe_path  = ""
out_path = ""

class SeqRec:
    pass

def generate_read_fraction_sequence_list(se_path, read_fraction, reads):
    #
    # Determine the number of reads in the input FASTA file.
    #
    print("Counting number of reads in '" + se_path + "'...", file=sys.stderr)

    if ftype == "gzfastq" or ftype == "gzfasta":
        line_cnt = count_gzip_lines(se_path)
    else:
        line_cnt = count_lines(se_path)

    if ftype == "gzfastq" or ftype == "fastq":
        records = line_cnt / 4.0
        if line_cnt % 4 != 0:
            print("Error: FASTQ file does not have the correct format.", file=sys.stderr)
            sys.exit()
    else:
        records = line_cnt / 2.0
        if line_cnt % 2 != 0:
            print("Error: FASTQ file does not have the correct format.", file=sys.stderr)
            sys.exit()

    read_cnt = int(records * read_fraction)
    print("  File has {} lines, {} records; sampling {}%, or {} reads.".format(line_cnt, records, read_fraction*100, read_cnt), file=sys.stderr)

    #
    # Generate list of single-end reads.
    #
    for i in range(0, int(records)):
        reads.append(False)
    #
    # Generate a series of random numbers representing reads to include. 
    #
    total = 0
    while total < read_cnt:
        read = random.randint(0, records - 1);
        if reads[read] == False: 
            reads[read] = True;
            total += 1;
    return

def generate_depth_sequence_list(se_path, paired, genome_size, depth_coverage, min_len_lim, max_len_lim, reads):
    #
    # Determine the number of reads in the input FASTA file.
    #
    print("Counting number of reads in '" + se_path + "'...", file=sys.stderr)

    #
    # Open the input file to measure read lengths.
    #
    if ftype == "gzfastq" or ftype == "gzfasta":
        fh = gzip.open(se_path, 'rb')
    else:
        fh = open(se_path, 'r')

    readlist  = []
    reads_len = 0
    read_cnt  = 0

    while True:
        s = next_seq(fh)
        if s == None:
            break
        slen = len(s.seq)
        if paired:
            slen *= 2
        readlist.append((s.id, slen))

        if slen > min_len_lim and slen < max_len_lim:
            reads_len += slen
        read_cnt  += 1

    total_cov = float(reads_len) / float(genome_size)
    print("  File has", read_cnt, "reads, " + str(reads_len) + "bp in total length, " + str(total_cov) + "x coverage.", file=sys.stderr)

    depth_cov_len = depth_coverage * genome_size

    print("Selecting reads to achieve " + str(depth_cov_len) + "bp in total length, " + str(depth_coverage) + "x coverage.", file=sys.stderr)
    
    #
    # Generate list of reads.
    #
    for i in range(read_cnt):
        reads.append(False)

    if reads_len < depth_cov_len:
        print("Error: not enough raw read length to generate a depth of coverage of " + str(depth_coverage) + "x.", file=sys.stderr)
        exit(1)

    #
    # Generate a series of random numbers representing reads to include. 
    #
    cur_len = 0
    while cur_len < depth_cov_len:
        read = random.randint(0, read_cnt - 1);
        if reads[read] == False and readlist[read][1] > min_len_lim and readlist[read][1] < max_len_lim: 
            reads[read] = True;
            cur_len += readlist[read][1];
    return
            
def write_dataset(se_path, pe_path, out_path, reads):
    global ftype

    #
    # Open the input files to sample from.
    #
    if ftype == "gzfastq" or ftype == "gzfasta":
        fh_1 = gzip.open(se_path, 'rb')
        if paired:
            fh_2 = gzip.open(pe_path, 'rb')
    else:
        fh_1 = open(se_path, 'r')
        if paired:
            fh_2 = open(pe_path, 'r')

    #
    # Check if the user specified an output file or an output directory
    #
    if os.path.isdir(out_path):
        if out_path.endswith("/") == False:
            out_path += "/"
        #
        # Open the output files to write sampled reads to.
        #
        pos_1  = se_path.rfind("/")
        pos_1 += 1
        se_out_path = out_path + se_path[pos_1:]

        if paired:
            pos_2  = pe_path.rfind("/")
            pos_2 += 1
            pe_out_path = out_path + pe_path[pos_2:]
    else:
        if paired == True:
            print("Error: cannot specify an output file with paired-end data, must specify an output directory.", file=sys.stderr)
            exit(1)
        se_out_path = out_path
            
    if ftype == "gzfastq" or ftype == "gzfasta":
        out_fh_1 = gzip.open(se_out_path, 'wb')
        if paired:
            out_fh_2 = gzip.open(pe_out_path, 'wb')
    else:
        out_fh_1 = open(se_out_path, 'w')
        if paired:
            out_fh_2 = open(pe_out_path, 'w')

    print("Sampling reads... ", file=sys.stderr, end="")
    for sample_read in reads:
        r_1 = next_seq(fh_1)
        if paired:
            r_2 = next_seq(fh_2)
        if sample_read == True:
            write_seq(out_fh_1, r_1)
            if paired:
                write_seq(out_fh_2, r_2)

    print("done.", file=sys.stderr)
    fh_1.close()
    out_fh_1.close()
    if paired:
        fh_2.close()
        out_fh_2.close()
    return

def next_seq(fh):
    global ftype

    line = fh.readline()
    if len(line) == 0:
        return None

    seq     = SeqRec()
    seq.id  = line
    seq.seq = fh.readline()
    if ftype == "fastq" or ftype == "gzfastq":
        seq.id2  = fh.readline()
        seq.qual = fh.readline()
    return seq

def write_seq(fh, seq):
    global ftype

    if ftype == "fastq" or ftype == "gzfastq":
        fh.write(seq.id)
        fh.write(seq.seq)
        fh.write(seq.id2)
        fh.write(seq.qual)
    else:
        fh.write(seq.id)
        fh.write(seq.seq)

def count_lines(path):
    i = -1
    with open(path, 'r') as fh:
        for i, l in enumerate(fh):
            pass
    return i + 1

def count_gzip_lines(path):
    i = -1
    fh = gzip.open(path, 'rb')
    for i, l in enumerate(fh):
        pass
    return i + 1

def parse_genome_size(sizestr):
    size    = sizestr.strip(' ')
    numsize = 0
    
    if size[-1] == 'G' or size[-1] == 'g':
        numsize = int(float(size[0:-1]) * 1000000000)
    elif size[-1] == 'M' or size[-1] == 'm':
        numsize = int(float(size[0:-1]) * 1000000)
    elif size[-1] == 'K' or size[-1] == 'k':
        numsize = int(float(size[0:-1]) * 1000)
    else:
        numsize = int(size)
    return numsize

def parse_command_line():
    global se_path
    global pe_path
    global out_path
    global read_fraction, depth_coverage, genome_size, min_len_lim, max_len_lim
    global ftype
    global paired
    global user_seed

    desc = '''Subsample a set of reads from one or a pair of FASTQ files. \
    One can either select a fraction of total reads to sample, or one can \
    specify a genome length and depth of coverage to sample to fill.'''
    p = argparse.ArgumentParser(description=desc)

    #
    # Add options.
    #
    p.add_argument("-1", "--se_path",
                   help="Path to single-end reads.")
    p.add_argument("-2", "--pe_path",
                   help="Path to paired-end reads.")
    p.add_argument("-o", "--out",
                   help="Path to write sampled data.")
    p.add_argument("-r", "--fraction",
                   help="Fraction of reads you want to randomly sample.")
    p.add_argument("-g", "--genome",
                   help="Approximate size of the genome for use in determining depth of coverage.")
    p.add_argument("-c", "--depth", type=float,
                   help="Depth of coverage to randomly sample to (based on specified genome size).")
    p.add_argument("-l", "--length",
                   help="Only consider reads longer than length limit for sampling.")
    p.add_argument("--max_length",
                   help="Only consider reads shorter than max_length limit for sampling.")
    p.add_argument("--seed", type=int,
                   help="Specify a seed to the random number generator to start this sampling.")

    #
    # Parse the command line
    #
    args = p.parse_args()

    if args.se_path != None:
        se_path = args.se_path
    if args.pe_path != None:
        pe_path = args.pe_path
    if args.out != None:
        out_path = args.out
    if args.fraction != None:
        read_fraction = float(args.fraction)
    if args.genome != None:
        genome_size = parse_genome_size(args.genome)
    if args.depth != None:
        depth_coverage = float(args.depth)
    if args.length != None:
        min_len_lim = parse_genome_size(args.length)
    if args.max_length != None:
        max_len_lim = parse_genome_size(args.max_length)
    if args.seed != None:
        user_seed = args.seed

    if len(se_path) == 0 or os.path.exists(se_path) == False:
        print("You must specify a valid path to at least one input file.", file=sys.stderr)
        p.print_help()
        sys.exit()

    if len(out_path) == 0:
        print("You must specify a valid path to write output files.", file=sys.stderr)
        p.print_help()
        sys.exit()

    if read_fraction == 0.0 and depth_coverage == 0:
        print("You must specify the fraction of reads you want to randomly sample or a depth of coverage/genome size.", file=sys.stderr)
        p.print_help()
        sys.exit()

    if read_fraction > 0.0 and depth_coverage > 0.0:
        print("You must specify the fraction of reads or depth of coverage, not both.", file=sys.stderr)
        p.print_help()
        sys.exit()
        
    if read_fraction > 1:
        read_fraction = read_fraction / 100

    pos = se_path.rfind(".")
    if se_path[pos:] == ".gz":
        pos = se_path.rfind(".", 0, pos)
    if se_path[pos:] == ".fastq.gz" or se_path[pos:] == ".fq.gz":
        ftype = "gzfastq"
    elif se_path[pos:] == ".fasta.gz" or se_path[pos:] == ".fa.gz":
        ftype = "gzfasta"
    elif se_path[pos:] == ".fastq" or se_path[pos:] == ".fq":
        ftype = "fastq"
    elif se_path[pos:] == ".fasta" or se_path[pos:] == ".fa":
        ftype = "fasta"

    if len(pe_path) > 0 and os.path.exists(pe_path):
        paired = True

# #                                                                                             # #
# # ------------------------------------------------------------------------------------------- # #
# #                                                                                             # #

parse_command_line()

print("File type is: " + ftype + ";", ("paired data detected" if paired else "single-end data detected"), file=sys.stderr)
print("Minimum length limit of " + str(min_len_lim) + " was specified.", file=sys.stderr)

if max_len_lim != 100000001:
    print("Maximum length limit of " + str(max_len_lim) + " was specified.", file=sys.stderr)

s = 0
if (user_seed > 0):
    s = user_seed
    random.seed(user_seed)
else:
    s = random.randrange(sys.maxsize)
    random.seed(s)

print("Random seed for this sampling: " + str(s) + "; specify --seed " + str(s) + " to reproduce this subsampling.")
reads = []

#
# Create a list of reads to include from the FASTA file.
#
if read_fraction > 0:
    generate_read_fraction_sequence_list(se_path, read_fraction, reads)
else:
    generate_depth_sequence_list(se_path, paired, genome_size, depth_coverage, min_len_lim, max_len_lim, reads)

write_dataset(se_path, pe_path, out_path, reads)
