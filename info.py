#!/usr/bin/env python3

import csv, sys, os.path, gzip, argparse, math

def is_valid_path(paths):
    ''' {Checking the validity of full table and their extensions}'''
    if type(paths) is list:
        path_dict = {}
        for path in paths:
           if not os.path.exists(path):
              sys.exit("\n'Error: {}': Oops! '{}' path doesn't exist!\n".format(path, path))
           if os.path.basename(path) not in path_dict.keys(): 
               path_dict[os.path.basename(path)] = 0
           if os.path.basename(path) in path_dict.keys():
               path_dict[os.path.basename(path)] += 1
        for key, value in path_dict.items():
            if value > 1:
               sys.exit("\n'Error: {}': Oops! the same '{}' file exists {} times in the command line!\n".format(key, key, value))
    if type(paths) is str and not os.path.exists(paths):
              sys.exit("\n'Error: {}': Oops! '{}' path doesn't exist!\n".format(path, path))


def make_dictionary(f):
    genome_dictionary = {}
    key = None
    for number, line in enumerate(f):
        if number % 2 == 0:
           line = line.strip('\n')
           key = line
        if number % 2 == 1:
            line  = line.strip('\n')
            value = line
            if key != None:
               genome_dictionary[key]= value
            key = None                       
    return genome_dictionary

def parse_busco_tables(path1, path2, genobus, compath):
    btable_gfile_dict = {}
    genobus_filename = os.path.basename(str(genobus.rstrip('/')))
    gfile_handle = open(genobus_filename, 'r')
    for line in gfile_handle:
        line=line.strip().strip('\n')
        files_list=line.split(',')
        btable = os.path.basename(files_list[0])
        btable_gfile_dict[btable]=files_list[1]
    gfile_handle.close()    
    complete_gene_info = [[] for i in range(len(path1))]
    fragmented_gene_info = []
    for index, pat in enumerate(path1):
            if pat.endswith('.gz'):
               full_table = gzip.open(pat, 'rt')
            else:
               full_table = open(pat)
            for line in full_table:
                if line[0] == '#':
                   continue
                fields = line.split()
                if fields[1] != 'Complete':
                   continue
                assert len(fields) == 7
                if len(fields) == 7:
                   gene_id = fields[0]
                   status = fields[1]
                   scaffold = fields[2]
                   start = int(fields[3])
                   end = int(fields[4])
                   gfp = btable_gfile_dict[os.path.basename(pat)]
                   t = (gene_id, status, scaffold, start, end, gfp)
                   complete_gene_info[index].append(t)

    if  path2.endswith('.gz'):
        full_table = gzip.open(path2, 'rt')
    else:
        full_table = open(path2)
    for line in full_table:
                if line[0] == '#':
                   continue
                fields = line.split()
                if fields[1] != 'Fragmented':
                   continue
                assert len(fields) == 7
                if len(fields) == 7:
                   gene_id = fields[0]
                   status = fields[1]
                   scaffold = fields[2]
                   start = int(fields[3])
                   end = int(fields[4])
                   t = (gene_id, status, scaffold, start, end)
                   fragmented_gene_info.append(t)                

    sorted_fragmented_gene_info = sorted(fragmented_gene_info)
    complete_gene_info_sorted_by_element_length = sorted(complete_gene_info, key=len, reverse = True)
    input_data = []
    comp_scaffold_dict = {}
    for index, item in enumerate(complete_gene_info_sorted_by_element_length):
        for gene, stat, scaff, star, en, gf in item:
            for gene_id, status, scaffold, start, end in sorted_fragmented_gene_info:
                rt = (gene_id, status, scaffold, start, end)
                t = (gene, scaff, star, en, scaffold, start, end)
                if gene_id == gene:
                    gf_handle = open(gf)
                    gf_dict = make_dictionary(gf_handle)
                    gf_handle.close()
                    deposite_compscaf = scaff
                    depo_dir = compath.rstrip('/')+'/'
                    depo_file_path = depo_dir+deposite_compscaf+'.fa'
                    if scaffold in comp_scaffold_dict:
                        comp_scaffold_dict[scaffold] += 1
                    if scaffold not in comp_scaffold_dict:
                        comp_scaffold_dict[scaffold] = 1
                    out = open(depo_file_path, 'w')
                    print('>'+scaff, file=out)
                    print(gf_dict['>'+scaff], file=out)
                    out.close()
                    #revread = reverse_complement(gf_dict['>'+scaff])
                    #deposite_revscaf = scaff
                    #rev_depo_path = revpath.rstrip('/')+'/'+deposite_revscaf+'Yes.fa'
                    #revout = open(rev_depo_path, 'w')
                    #print('>'+scaff, file=revout)
                    #print(revread, file=revout)
                    #revout.close()
                    input_data.append(t)
                    sorted_fragmented_gene_info.remove(rt)
    print(comp_scaffold_dict)
    return input_data


def findsource(datafile, path1, path2, revpath):
    incscaf_handle = open(path1)
    incscaf_dict = make_dictionary(incscaf_handle)
    incscaf_handle.close()
    data_handle = open(datafile.rstrip('/'), 'r')
    for item in data_handle:
        if item.startswith('BUSCO_name'):
           continue
        scaffold_info=item.strip('\n').split(',')
        inc_scaffold=scaffold_info[4]
        incscaffold = inc_scaffold
        inc_depo_path = path2.rstrip('/')+'/'+incscaffold+'.fa'
        incout = open(inc_depo_path, 'w')
        print('>'+incscaffold, file=incout)
        print(incscaf_dict[incscaffold], file=incout)
        incout.close() 
        scaff = incscaffold
        revread = reverse_complement(incscaf_dict[scaff])
        deposite_revscaf = scaff
        rev_depo_path = revpath.rstrip('/')+'/'+deposite_revscaf+'Yes.fa'
        revout = open(rev_depo_path, 'w')
        print('>'+scaff, file=revout)
        print(revread, file=revout)
        revout.close()
    data_handle.close()
    return "Scaffold with incomplete BUSCO extracted"

def reverse_complement(read):
    complementarybases={'A':'T', 'C':'G', 'T':'A', 'G':'C', 'a':'t', 'c':'g', 't':'a', 'g':'c', 'N':'N'}
    complementarylist=[]
    for base in read[::-1]:
        if base == 'T'or 'A' or 'G' or 'C' or 't' or 'a' or 'g' or 'c'or 'N':
            complementarylist.append(complementarybases[base])
    return ''.join(complementarylist)

def read_fasta(fh, fg):
    currentline = " "
    for line in fh:
        if  line.startswith('>'):
            line = line.lstrip('>')
            line = line.rstrip('\n')
            if currentline != " ": 
                currentlines = currentline.lstrip()
                fg.write(currentlines+'\n')
            fg.write(line+'\n')
            currentline = " "
        else:
            line = line.strip()
            line = line.rstrip('\n')
            currentline = currentline + line        
    currentline = currentline.strip()         
    fg.write(currentline)


parser = argparse.ArgumentParser(
    description= 'Extract information from BUSCO tables', usage='%(prog)s [options]')
parser = argparse.ArgumentParser(
    description= 'Parse BUSCO tables', usage='%(prog)s [options]') 
parser.add_argument(
    '-a', '--fragmentedbt', required = True, nargs = '?', metavar = 'BUSCO_TABLE', help = 'full_table path')    
parser.add_argument(
    '-b','--completebt', required = True, nargs = '+', metavar = 'BUSCO_TABLE', help= 'full_table path')    
parser.add_argument(
    "-i", "--input", required = True, metavar = 'GENOME_FILE', help = 'path to genome file')
parser.add_argument(
    "-j", "--single", required = True, metavar = 'GENOME_WITH_SINGLE_LINE_SEQ_FOR_EACH_SCAFFOLD',
    help = 'path to the file as output when multiline input converted to single line output')
parser.add_argument(
    '-d', '--data', required = True, metavar = 'DATA_FILE',
    help = 'path to the full data set file to process' )      
parser.add_argument(
    '-c', '--complete', required = True, metavar = 'COMPLETE_SCAFFOLD_FILE',
    help = 'path to directory of files of scaffolds with complete busco ortholog')   
parser.add_argument(
    '-f', '--fragmented', required = True, metavar = 'INCOMPLETE_SCAFFOLD_FILE',
    help = 'path to directory of files of scaffolds with fragmented busco ortholog')
parser.add_argument(
    '-r', '--revcomp', required = True, metavar = 'REVERSE_COMPLEMENTED_SCAFFOLD_FILE', 
    help = 'path to directory of reverse complement scaffolds with complete busco ortholog')
parser.add_argument(
    '-n', '--network', required = True, metavar = 'NETWORK_FILE', help = 'path to network file')

#Creat data file
args = parser.parse_args()
print(args)
assert is_valid_path(args.completebt) == None
assert is_valid_path(args.fragmentedbt) == None
print('\nprocessing your full_table file(s)......................\n')
data = parse_busco_tables(args.completebt, args.fragmentedbt, args.network, args.complete)
var = str(os.path.basename(args.data))
var = var.strip('.csv')
var = var + '.csv'
out = open(var, 'w')
print("{},{},{},{},{},{},{}".format('BUSCO_name','Scaffold_com','Start_com','End_com','Scaffold_inc','Start_inc','End_inc'), sep = ',', file=out)
for gene, scaff, star, en, scaffold, start, end in data:
    print(gene, scaff, star, en, scaffold, start, end, sep = ',', file = out)
out.close()

in_f = args.input.rstrip('/')
f_h = open(in_f, 'r')
f = open(args.single.rstrip('/'), 'w')
read_fasta(f_h, f) 
f.close() 

findsource(args.data, args.single, args.fragmented, args.revcomp)

