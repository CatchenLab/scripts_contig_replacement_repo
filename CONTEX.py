#!/usr/bin/env python3

import argparse
import os
import re
import gzip
import csv
import math
import itertools
import string
import numpy as np



#Usage - replace fragmented busco with its complete version

#Docstring format - Numpydoc 
'''
Description of what function does

Parameters
------------
argument: expected type of argument
          Description of argument

Returns
------------
The type of value return: can include description of the return value
'''

def sequence_filter(file_handle):
    '''
    It captures the sequence by filtering the fasta header
    Parameter
    ---------
    file_handle: It's a fasta file object

    Returns
    ---------
    String: The string is the sequence of nucleotides of single scaffold in the fasta file            
    '''
    currentline = ''
    for line in file_handle:
        if line.startswith('>'):
           continue
        line=line.strip()
        line=line.strip('\n')
        currentline = currentline + line
    return currentline

tab = str.maketrans("ACTG", "TGAC")

def reverse_complement_table(seq):
    '''
    It reverse complement the sequence of nucleotide
    Parameter
    ---------
    read: It's a sequence of nucleotide

    Returns
    ---------
    String: The string is the reverse complement of the read
    '''
    return seq.translate(tab)[::-1]

def reverse_complement(read):
    '''
    It reverse complement the sequence of nucleotide
    Parameter
    ---------
    read: It's a sequence of nucleotide

    Returns
    ---------
    String: The string is the reverse complement of the read
    '''
    complementarybases={'A':'T', 'C':'G', 'T':'A', 'G':'C', 'a':'t', 'c':'g', 't':'a', 'g':'c', 'N':'N'}
    complementarylist=[]
    for base in read[::-1]:
        if base == 'T'or 'A' or 'G' or 'C' or 't' or 'a' or 'g' or 'c'or 'N':
            complementarylist.append(complementarybases[base])
    return ''.join(complementarylist)

def kmerize(seq, k):
    '''
    It generates a collection of kmers of size 'k' from a sequence (seq) of nucleotides
    Parameter
    ---------
    seq: sequence of nucleotide
    k: kmer size

    Returns
    --------  
    Dictionary: The collection of the unique kmers without Ns and palindrom as well as repeated kmers
    '''
    kmers_dict = {}
    unique_kmer_dict = {}
    for i in range(len(seq)-k+1):
        kmer = seq[i:i+k]
        if kmer in kmers_dict:
           kmers_dict[kmer].append(i+1) 
        if kmer not in kmers_dict:
            kmers_dict[kmer] = [i+1]
    for kmer, number_list in kmers_dict.items():
        if len(number_list) > 1:
            continue
        if 'N' in kmer:
            continue
        if kmer == reverse_complement_table(kmer):
            continue
        else:
            unique_kmer_dict[kmer] = number_list
    return unique_kmer_dict

def read_fasta(fh, fg):
    '''
    It converts multi-line fasta sequence to single-line

    Parameters
    -----------
    fh: multi-line fasta file object to read
    fg: single-line fasta file object to write

    Returns
    -----------
    Strings: Writes fasta headers and their single-line nucleotide sequences in single-line fasta file object 
    '''
    dict = {}
    scaf = None
    prev_scaf = None
    for line in fh:
        if  line.startswith('>'):
            line = line.lstrip('>').rstrip('\n')
            scaf = line
            if scaf not in dict:
               dict[scaf] = []
               if prev_scaf != None:
                    sequence = ''.join(dict[prev_scaf])
                    fg.write(prev_scaf+'\n')
                    fg.write(sequence+'\n')
                    prev_scaf = line
                    sequence = ''
               else:
                    prev_scaf = line
            else:
                 pass
        else:
             line = line.strip().rstrip('\n')
             dict[scaf].append(line)
    seq = ''.join(dict[prev_scaf])
    fg.write(prev_scaf+'\n')
    fg.write(seq) 

def make_dictionary(f):
    '''
    It converts fasta file content in dictionary where key == header & value == sequence
    Parameter
    ---------
    f: fasta file object of single-line fasta file 

    Returns
    ---------
    Dictionary: The dictionary with keys == headers & values == sequences
    '''
    genome_dictionary = {}
    key = None
    for number, line in enumerate(f):
        if number % 2 == 0:
           line = line.strip('\n')
           key = line
        if number % 2 == 1:
            line  = line.strip('\n')
            value = line
            if key != None:
               genome_dictionary[key]= value
            key = None                       
    return genome_dictionary

def parse_busco_tables(busco_paths):
    '''
    It parses busco full_table_*.tsv file in a list 
    Parameter
    ---------
    busco_paths: iterable, list of busco file path(s)

    Returns
    ---------
    Dictionary: the dictionary with key as scaffold plus related busco table name and value as list 
    of tuples. Each tuple consists of name of the busco i.e. the gene id, the status, the name of 
    the scaffold having the busco, the start, and the end position of the busco. The input 
    busco_paths should be iterable list of file name(s)'''
    busco_gene_info = {}
    for index in range(len(busco_paths)):
        if busco_paths[index].endswith('.gz'):
           full_table = gzip.open(busco_paths[index], 'rt')
        else:
            full_table = open(busco_paths[index], 'r')
            for line in full_table:
                if line[0] == '#':
                   continue
                fields = line.split()
                if fields[1] == 'Missing':
                   continue
                assert len(fields) == 7
                if len(fields) == 7:
                    gene_id = fields[0]
                    status = fields[1]
                    scaffold = fields[2]
                    start = int(fields[3])
                    end = int(fields[4])
                    gene_status_scaffold_start_end_tuple  = (gene_id, status, scaffold, start, end)
                    paths = os.path.basename(busco_paths[index])
                    if scaffold + '-' + os.path.basename(busco_paths[index]) in busco_gene_info:
                       busco_gene_info[scaffold + '-' + paths].append(gene_status_scaffold_start_end_tuple)
                    if scaffold + '-' + os.path.basename(busco_paths[index]) not in busco_gene_info:
                        busco_gene_info[scaffold + '-' + paths] = [gene_status_scaffold_start_end_tuple]
    return busco_gene_info

def parse_rev_busco_table(busco_paths):
    busco_gene_info = {}
    for index in range(len(busco_paths)):
        if busco_paths[index].endswith('.gz'):
           full_table = gzip.open(busco_paths[index], 'rt')
        else:
            full_table = open(busco_paths[index], 'r')
            for line in full_table:
                if line[0] == '#':
                   continue
                fields = line.split()
                gene_id = fields[0]
                status = fields[1]
                busco_gene_info[gene_id] = status
    return busco_gene_info



def directions(fln, frn, cln, crn, flcld, frcrd, flcrd, frcld):
    '''It returns the direction of busco
    Parameters
    ----------
    fln: a list of left-side neighbors of fragmented busco
    frn: a list of right-side neigbors of fragmented busco
    cln: a list of left-side neighbors of complete busco
    crn: a list of right-side neighbors of complete busco
    flcld, frcrd, flcrd, frcld: booleans indicating whether fragmented and complete
    buscos does not have common neighboring buscos

    Returns:
    --------
    String: direction of the busco (same-direction, 'reverse-complemented' or 'unresolvable 
    due to complex synteny')
    '''
    direction = ''
    #f(!=0 and !=-1) and c(!=0 and !=-1), where f & c are the position of fragmented & 
    # complete buscos, respectively, in ordered lists.
    if  (bool(fln) == True and 
        bool(frn) == True and 
        bool(cln)  == True and
        bool(crn) == True):
        if (flcld == False and frcrd == False):
            if (flcrd == True and frcld == True):
               direction += 'same-direction based on synteny'   
            else:
                direction += 'unresolvable due to complex synteny'
        elif  (flcrd == False and frcld == False):
            if (flcld == True and frcrd == True):
               direction += 'opposite-direction based on synteny'
            else: 
                direction += 'unresolvable due to complex synteny '
        else:
            direction += 'unresolvable due to complex synteny'
    #f(!=0 and !=-1) and c=0
    elif (bool(fln) == True and 
         bool(frn) == True and
         bool(cln)  == False and
         bool(crn) == True):
         if frcrd == False and flcrd == True:
            direction += 'same-direction based on synteny'
         elif flcrd == True and frcrd == False:
              direction += 'opposite direction based on synteny'
         else:
             direction += 'unresolvable due to complex synteny'   
    
    #f(!=0 and !=-1) and c=-1
    elif (bool(fln) == True and 
         bool(frn) == True and
         bool(cln)  == True and
         bool(crn) == False):
         if flcld == False and frcld == True:
            direction += 'same-direction based on synteny'
         elif frcld == False and flcld == True:
              direction += 'opposite-direction based on synteny'
         else:
             direction += 'unresolvable due to complex synteny' 
    
    #f=0 and c=0
    elif (bool(fln) == False and 
         bool(frn) == True and 
         bool(cln)  == False and
         bool(crn) == True):
         if frcrd == False:
            direction += 'same-direction based on synteny'
         else:
             direction += 'unresolvable due to complex synteny'
             
    #f=0 and c=-1
    elif (bool(fln) == False and
         bool(frn) == True and
         bool(cln)  == True and 
         bool(crn) == False):
         if frcld == False:
             direction += 'opposite-direction based on synteny'
         else:
             direction += 'unresolvable due to complex synteny'
    
    #f=0 and c(!=0 and !=-1)
    elif (bool(fln) == False and
         bool(frn) == True and
         bool(cln)  == True and
         bool(crn) == True):
         if frcrd == False and frcld == True:
            direction += 'same-direction based on synteny'
         elif frcrd == True and frcld == False:
            direction += 'opposite-direction based on synteny' 
         else:
            direction += 'unresolvable due to complex synteny'  

    #f=-1 and c=-1
    elif (bool(fln) == True and 
         bool(frn) == False and 
         bool(cln)  == True and
         bool(crn) == False):
         if flcld == False:
            direction += 'same-direction based on synteny'
         else:
             direction += 'unresolvable due to complex synteny'
    
    #f=-1 and c=0:
    elif (bool(fln) == True and
         bool(frn) == False and
         bool(cln)  == False and
         bool(crn) == True):
         if flcrd == False:
            direction += 'opposite-direction based on synteny' 
         else:
             direction += 'unresolvable due to complex synteny'   

    #f=-1 and c(!=0 and !=-1)
    elif (bool(fln) == True and
         bool(frn) == False and
         bool(cln)  == True and
         bool(crn) == True):
         if flcld == False and flcrd == True:
            direction += 'same-direction based on synteny'
         elif flcrd == False and flcld == True:
             direction += 'opposite-direction based on synteny'   
         else:
              direction += 'unresolvable due to complex synteny'
    
    
    else:
        direction += 'unresolvable'

    return direction

def diff_start_end(start_list, end_list, index):
    diff_list = []
    for i in range(len(start_list)):
        if i == 0:
            diff_list.append(True)
        else:
            diff_start_end = start_list[i] - end_list[i-1]
            if diff_start_end > 0:
                diff_list.append(True)
            else:
                diff_list.append(False)
    return diff_list

def diff_list_dict(busco_list, diff_list):
    diff_dict = {}
    for index, busco in busco_list:
        diff_dict[busco] = diff_list[index]
    return diff_dict

def overlap_check(diff_list, index):
    overlap = None
    for ind, latest in enumerate(diff_list):
        if latest == False and ind == index:
            overlap = True
        elif latest == False and ind == index+1:
            overlap = True
        elif latest == False and ind == index-1:
            overlap = True
        else:      
            overlap = False
    return overlap

parser = argparse.ArgumentParser(
    description= 'Replace contigs having fragmented BUSCOs', usage='%(prog)s [options]')
parser.add_argument(
    "-i", "--input", required = True, metavar = 'GENOME_FILE', help = 'path to genome file')
parser.add_argument(
    "-j", "--single", required = True, metavar = 'GENOME_WITH_SINGLE_LINE_SEQ_FOR_EACH_SCAFFOLD',
    help = 'path to the file as output when multiline input converted to single line output')
parser.add_argument(
    '-a', '--fragmentedbt', required = True, nargs = '+', metavar = 'BUSCO_TABLE', help = 'full_table path')    
parser.add_argument(
    '-b','--completebt', required = True, nargs = '+', metavar = 'BUSCO_TABLE', help= 'full_table path')
parser.add_argument(
    '-x','--revcompbt', required = True, nargs = '+', metavar = 'BUSCO_TABLE', help= 'full_table path')    
parser.add_argument(
    '-o', '--output', required = True, 
    metavar = 'EDITED_GENOME_WITH_SINGLE_LINE_SEQ_FOR_EACH_SCAFFOLD', help= 'output file path')            
parser.add_argument(
    '-d', '--data', required = True, metavar = 'DATA_FILE',
    help = 'path to the full data set file to process' ) 
parser.add_argument(
    '-k', '--kmer', required = True, metavar = 'KMER_SIZE', type = int,
    help = 'choose size of kmer you want to search')
parser.add_argument(
    '-l','--leap', required = True, metavar = 'LEAP_NUMBER', type = int,
    help= 'move away from start and end positions of complete gene')    
parser.add_argument(
    '-c', '--complete', required = True, metavar = 'COMPLETE_SCAFFOLD_FILE',
    help = 'path to directory of files of scaffolds with complete busco ortholog')   
parser.add_argument(
    '-f', '--fragmented', required = True, metavar = 'INCOMPLETE_SCAFFOLD_FILE',
    help = 'path to directory of files of scaffolds with fragmented busco ortholog')
parser.add_argument(
    '-r', '--revcomp', required = True, metavar = 'REVERSE_COMPLEMENTED_SCAFFOLD_FILE', 
    help = 'path to directory of reverse complement scaffolds with complete busco ortholog')
parser.add_argument('--contig', action='store_true')
parser.add_argument('--nonsyntenic', action='store_true')
parser.add_argument('--shared', nargs='?', const=1, type=int, default=20)


args = parser.parse_args()

class completeBUSCO():
        
        def __init__(self, busco, cscaffold, start, end, inscaffold, instart, inend):
            ''' Data encapsulated in the class CompleteBUSCO for the method .gene() to operate on them'''

            ''' Name of the BUSCO'''
            self.busco=busco

            ''' Name of the scaffold with complete BUSCO'''
            self.cscaffold=cscaffold     

            ''' Start position of the complete BUSCO''' 
            self.start=start

            ''' End position of the fragmented BUSCO'''                 
            self.end=end

            ''' Name of the scaffold with fragmented BUSCO (inc=incomplete=fragmented)'''
            self.inscaffold=inscaffold

            ''' Start position of the fragmented BUSCO''' 
            self.instart=instart

            ''' End position of the fragmented BUSCO'''           
            self.inend=inend  
        
        def gene(self):

            ''' Obtain the scaffold sequence with complete gene '''
            comp_gene_scaffold_file = args.complete.strip('/') + '/' + self.cscaffold +'.fa'
            comp_gene_scaffold_file_handle = open(comp_gene_scaffold_file, 'r')
            comp_scaffold_str = sequence_filter(comp_gene_scaffold_file_handle)
            
            ''' Obtain the scaffold sequence with fragmented gene '''
            inc_gene_file = args.fragmented.strip('/') + '/' + self.inscaffold +'.fa'
            inc_gene_file_handle = open(inc_gene_file, 'r')
            inc_scaffold_str = sequence_filter(inc_gene_file_handle)

            '''Cut complete gene from the scaffold, then reverse complement it'''
            comp_string = str(comp_scaffold_str.strip('\n'))
           
            '''Cut fragmented gene from the scaffold, then reverse complement it'''
            inc_string=inc_scaffold_str.strip('\n') 

            ''' Complete gene left flanking sequence list with start and end positions '''
            comp_scaffold_start_str_end=list()

            ''' Complete gene right flanking sequence list with start and end positions '''
            comp_scaffold_start2_str_end2=list()
            
            
            '''Make positive number of leaps (args.leap) for non-reverse complemented scaffold with complete busco'''
            comp_scaffold_start_str_end_dict = {}
            comp_scaffold_start2_str_end2_dict = {}

            length_comp_scaffold = len(comp_scaffold_str)
            for i in range(int(args.leap)):
                if (((length_comp_scaffold  - self.end) == 0 and
                   (self.start - 1) == 0) or 
                   ((0 < (length_comp_scaffold  - self.end) < int(args.kmer)) and 
                   (0 < (self.start - 1) < int(args.kmer)))): 
                   pass
              
                elif ((0 < (length_comp_scaffold  - self.end) < int(args.kmer)) and
                    ((self.start - 1) == 0)): 
                    pass

                elif ((0 < (self.start - 1) < int(args.kmer)) and
                    (length_comp_scaffold  - self.end) == 0):
                    pass

                elif ((0 < (length_comp_scaffold  - self.end) < int(args.kmer)) and
                    ((self.start - 1) == int(args.kmer) or (self.start - 1) > int(args.kmer))):
                    left_flank_end = (self.start - 1) - i
                    left_flank_start = left_flank_end - int(args.kmer)
                    if (left_flank_start > 0 ) and left_flank_end > 0:
                       left_flank_seq = comp_scaffold_str[left_flank_start:left_flank_end] 
                       left_flank_seq = left_flank_seq.upper()
                       Tups = (left_flank_start+1, left_flank_seq, left_flank_end)
                       comp_scaffold_start_str_end.append(Tups)
                       comp_scaffold_start_str_end_dict[left_flank_end]=[Tups]

                elif (((length_comp_scaffold  - self.end) == int(args.kmer) or
                    (length_comp_scaffold  - self.end) > int(args.kmer)) and
                    (0 < (self.start - 1) < int(args.kmer))):
                    right_flank_start = self.end + 1 + i
                    right_flank_end = right_flank_start + (int(args.kmer) - 1)
                    right_flank_seq = comp_scaffold_str[(right_flank_start-1):right_flank_end]
                    right_flank_seq = right_flank_seq.upper()
                    Tups = (right_flank_start, right_flank_seq, right_flank_end)
                    comp_scaffold_start2_str_end2.append(Tups)
                    comp_scaffold_start2_str_end2_dict[right_flank_seq]=[Tups]

                elif (((length_comp_scaffold  - self.end) == int(args.kmer) or
                    (length_comp_scaffold  - self.end) > int(args.kmer) ) and
                    ((self.start - 1) == 0)):
                    right_flank_start = self.end + 1 + i
                    right_flank_end = right_flank_start + (int(args.kmer) - 1)
                    right_flank_seq = comp_scaffold_str[(right_flank_start-1):right_flank_end]
                    right_flank_seq = right_flank_seq.upper()
                    Tups = (right_flank_start, right_flank_seq, right_flank_end)
                    comp_scaffold_start2_str_end2.append(Tups)
                    comp_scaffold_start2_str_end2_dict[right_flank_seq]=[Tups]

                elif (((length_comp_scaffold  - self.end) == 0) and
                    ((self.start - 1) == int(args.kmer) or
                    (self.start - 1) > int(args.kmer))):
                    left_flank_end = (self.start - 1) - i
                    left_flank_start = left_flank_end - int(args.kmer)
                    if (left_flank_start > 0 ) and left_flank_end > 0:
                      left_flank_seq = comp_scaffold_str[left_flank_start:left_flank_end]
                      left_flank_seq = left_flank_seq.upper()
                      Tups = (left_flank_start+1, left_flank_seq, left_flank_end)
                      comp_scaffold_start_str_end.append(Tups) 
                      comp_scaffold_start_str_end_dict[left_flank_seq]=[Tups]     

                elif (((length_comp_scaffold  - self.end) == int(args.kmer) or
                    (length_comp_scaffold  - self.end) > int(args.kmer) ) and
                    ((self.start - 1) == int(args.kmer) or (self.start - 1) > int(args.kmer))):
                    left_flank_end = (self.start - 1) - i
                    left_flank_start = left_flank_end - int(args.kmer)
                    if (left_flank_start > 0 ) and left_flank_end > 0:
                      left_flank_seq = comp_scaffold_str[left_flank_start:left_flank_end]
                      left_flank_seq = left_flank_seq.upper()
                      Tups = (left_flank_start+1, left_flank_seq, left_flank_end)
                      comp_scaffold_start_str_end.append(Tups)
                    right_flank_start = self.end + 1 + i
                    right_flank_end = right_flank_start + (int(args.kmer) - 1)
                    right_flank_seq = comp_scaffold_str[(right_flank_start-1):right_flank_end]
                    right_flank_seq = right_flank_seq.upper()
                    Tups = (right_flank_start, right_flank_seq, right_flank_end)
                    comp_scaffold_start2_str_end2.append(Tups)
                    comp_scaffold_start2_str_end2_dict[right_flank_seq]=[Tups]
            
            comp_scaffold_start_str_end_sorted = sorted(comp_scaffold_start_str_end)
            '''Check common kmers between kmers from complete and incomplete BUSCO sets'''
            comp_scaffold_common_start_end=list()
            comp_scaffold_common_start2_end2=list()
            inc_scaffold_kmer_keys=list()
            inc_scaffold_kmer_keys2=list()

            ''' List the incomplete and complete busco and their neighbors in order'''
            fragmented_busco_parse = parse_busco_tables(args.fragmentedbt)
            path_frag = os.path.basename(args.fragmentedbt[len(args.fragmentedbt) - 1])
            sorted_tuple_list = sorted(fragmented_busco_parse[self.inscaffold + '-' + path_frag], key = lambda x: x[3])
            complete_busco_parse = parse_busco_tables(args.completebt)
            sorted_tuple_list_comp = []
            for index in range(len(args.completebt)):
                path_comp = os.path.basename(args.completebt[index])
                scaf_path = self.cscaffold + '-' + path_comp
                if scaf_path not in complete_busco_parse:
                    continue
                busco_names = [busco for busco, status, scaffold, start, end in complete_busco_parse[scaf_path][:]]
                if self.busco not in busco_names:
                    continue
                sorted_tuple_list_comp += sorted(complete_busco_parse[scaf_path], key = lambda x: x[3])
                break
            scaffold_frag_neighbors = [busco for busco, status, scaffold, start, end in sorted_tuple_list]
            scaffold_frag_start_pos = [start for busco, status, scaffold, start, end in sorted_tuple_list]
            scaffold_frag_end_pos = [end for busco, status, scaffold, start, end in sorted_tuple_list]    
            scaffold_comp_neighbors = [busco for busco, status, scaffold, start, end in sorted_tuple_list_comp]

            
            '''Find the index of the incomplete and complete busco'''
            frag_busco_index = scaffold_frag_neighbors.index(self.busco)
            comp_busco_index = scaffold_comp_neighbors.index(self.busco)

            '''Account for the direction of busco''' 
            inc_scaffold_rev_comp_str = reverse_complement_table(inc_scaffold_str.strip('\n'))
            inc_scaffold_kmers = kmerize(inc_scaffold_rev_comp_str, int(args.kmer))
            inc_scaffold_kmers_nr = kmerize(inc_scaffold_str.strip('\n'), int(args.kmer))
            comp_scaffold_kmers = kmerize(comp_scaffold_str.strip('\n'), int(args.kmer))
            direction = ''
            if (((len(scaffold_frag_neighbors) == 2 or len(scaffold_frag_neighbors) > 2) and 
               (len(scaffold_comp_neighbors) == 2 or len(scaffold_comp_neighbors) > 2))):
                flne = scaffold_frag_neighbors[:frag_busco_index]
                frne = scaffold_frag_neighbors[(frag_busco_index+1):]
                clne = scaffold_comp_neighbors[:comp_busco_index]
                crne = scaffold_comp_neighbors[(comp_busco_index+1):]
                flclu = set(flne).isdisjoint(clne)
                frcru = set(frne).isdisjoint(crne)
                flcru =  set(flne).isdisjoint(crne)
                frclu = set(frne).isdisjoint(clne)
                if (len(scaffold_frag_neighbors) == len(scaffold_comp_neighbors)):
                    if (scaffold_frag_neighbors == scaffold_comp_neighbors): 
                       direction += 'same-direction based on synteny'
                    if (scaffold_frag_neighbors == scaffold_comp_neighbors[::-1]):
                       direction += 'opposite-direction based on synteny'
                    if ((scaffold_frag_neighbors != scaffold_comp_neighbors) and 
                       (scaffold_frag_neighbors != scaffold_comp_neighbors[::-1])):
                        if (len(scaffold_frag_neighbors) == 2 and len(scaffold_comp_neighbors) == 2):
                           direction += 'unresolvable due to complex synteny'
                        else:
                            direction += directions(flne, frne, clne, crne, flclu, frcru, flcru, frclu)
                
                if (len(scaffold_frag_neighbors) != len(scaffold_comp_neighbors)):
                    direction += directions(flne, frne, clne, crne, flclu, frcru, flcru, frclu)
            
            else:
                if args.nonsyntenic:
                    same_type_list = []
                    opposite_type_list = []
                    difference = self.inend - self.instart
                    incomplete_start=len(inc_scaffold_str.strip('\n')) - self.inend + 1
                    incomplete_end=incomplete_start + difference
                    inc_scaffold_kmer_keys_list = list(inc_scaffold_kmers.keys())
                    inc_scaffold_kmer_keys_list_to_set = set(inc_scaffold_kmer_keys_list)
                    inc_scaffold_kmer_keys_nr_list = list(inc_scaffold_kmers_nr.keys())
                    inc_scaffold_kmer_keys_nr_list_to_set = set(inc_scaffold_kmer_keys_nr_list)
                    comp_scaffold_start_str_end_dict_to_list = list(comp_scaffold_start_str_end_dict.keys())
                    comp_scaffold_start_str_end_list_to_set = set(comp_scaffold_start_str_end_dict_to_list)
                    comp_scaffold_start2_str_end2_dict_to_list = list(comp_scaffold_start2_str_end2_dict.keys())
                    comp_scaffold_start2_str_end2_list_to_set = set(comp_scaffold_start2_str_end2_dict_to_list)
                    comp_scaffold_kmers_keys_list = list(comp_scaffold_kmers.keys())
                    comp_scaffold_kmers_keys_list_to_set = set(comp_scaffold_kmers_keys_list)
        
                    if ((comp_scaffold_start_str_end_list_to_set.isdisjoint(inc_scaffold_kmer_keys_list_to_set) == False) and 
                       (comp_scaffold_start_str_end_list_to_set.isdisjoint(inc_scaffold_kmer_keys_nr_list_to_set) == True)):
                            
                    #for startpos, sequence, endpos in comp_scaffold_start_str_end_sorted:
                        #if inc_scaffold_kmers.get(sequence) != None:
                            direction = "opposite-direction based on mapping of flanking sequence"
                            TypeI = "\nleft-flanking sequence mapped to reverse-complemented scaffold"
                            opposite_type_list.append(TypeI)
                            #print(TypeI, 'at ',inc_scaffold_kmers[sequence], 
                            #'whereas fragmented gene starts at', incomplete_start)
                            #print("left-flanking sequence", 'start at ',startpos, '& end at ', endpos)
                            #break
                    if  ((comp_scaffold_start2_str_end2_list_to_set.isdisjoint(inc_scaffold_kmer_keys_list_to_set) == False) and
                        (comp_scaffold_start2_str_end2_list_to_set.isdisjoint(inc_scaffold_kmer_keys_nr_list_to_set) == True)):
                            
                    #for startpos, sequence, endpos in comp_scaffold_start2_str_end2:
                        #if inc_scaffold_kmers.get(sequence) != None:
                            direction = "opposite-direction based on mapping of flanking sequence"
                            TypeII = "\nright-flanking sequence mapped to reverse-complemented scaffold"
                            opposite_type_list.append(TypeII)
                            #print(TypeII, 'at ',inc_scaffold_kmers[sequence], 
                            #'whereas fragmented gene ends at ', incomplete_end)
                            #print("right-flanking sequence", 'start at ',startpos, ' & end at ',endpos)
                            #break
                    if  ((comp_scaffold_start_str_end_list_to_set.isdisjoint(inc_scaffold_kmer_keys_nr_list_to_set) == False) and
                        (comp_scaffold_start_str_end_list_to_set.isdisjoint(inc_scaffold_kmer_keys_list_to_set) == True)):
                    #for startpos, sequence, endpos in comp_scaffold_start_str_end_sorted:
                        #if inc_scaffold_kmers_nr.get(sequence) != None:
                            direction = "same-direction based on mapping of flanking sequence"
                            TypeIII = "\nleft-flanking sequence mapped to non-reverse-complemented scaffold"
                            same_type_list.append(TypeIII)
                            #print(TypeIII, 'at ', inc_scaffold_kmers_nr[sequence], 'whereas fragmented gene starts at',
                            #self.instart)
                            #print("left-flanking sequence", 'start at', startpos, '& end at ', endpos)
                            #break
                    if  ((comp_scaffold_start2_str_end2_list_to_set.isdisjoint(inc_scaffold_kmer_keys_nr_list_to_set) == False) and
                        (comp_scaffold_start2_str_end2_list_to_set.isdisjoint(inc_scaffold_kmer_keys_list_to_set) == True)):
                    #for startpos, sequence, endpos in comp_scaffold_start2_str_end2:
                        #if inc_scaffold_kmers_nr.get(sequence) != None:
                            direction = "same-direction based on mapping of flanking sequence"
                            TypeIV = "\nright-flanking sequence mapped to non-reverse-complemented scaffold"
                            same_type_list.append(TypeIV)
                            #print(TypeIV, 'at ',inc_scaffold_kmers_nr[sequence], 'whereas fragmented gene ends at',
                            #self.inend)
                            #print("right-flanking sequence", 'start at', startpos, '& end at ',endpos)
                            #break
                    if (len(same_type_list) != 0 and len(opposite_type_list) != 0):
                        direction = 'Undetermined based on mapping'
                    if (len(same_type_list) == 0 and len(opposite_type_list) == 0):
                        direction = 'Undetermined based on mapping'
            
            if direction == 'Undetermined based on mapping':
                        same_type_list = []
                        opposite_type_list = []
                        if (inc_scaffold_kmer_keys_list_to_set.isdisjoint(comp_scaffold_kmers_keys_list_to_set) == False and
                            inc_scaffold_kmer_keys_nr_list_to_set.isdisjoint(comp_scaffold_kmers_keys_list_to_set) == True):
                            num = len(inc_scaffold_kmer_keys_list_to_set.intersection(comp_scaffold_kmers_keys_list_to_set))
                            per = (num/len(comp_scaffold_kmers_keys_list_to_set))*100
                            if per > args.shared:   
                               direction = "opposite-direction based on mapping of scaffold to scaffold kmer"
                               TypeV = "\nsequence mapped to reverse-complemented scaffold"
                               opposite_type_list.append(TypeV)
                            else:
                                direction = "Undetermined based on scaffold to scaffold mapping"
                        if (inc_scaffold_kmer_keys_nr_list_to_set.isdisjoint(comp_scaffold_kmers_keys_list_to_set) == False and 
                            inc_scaffold_kmer_keys_list_to_set.isdisjoint(comp_scaffold_kmers_keys_list_to_set) == True):
                            num = len(inc_scaffold_kmer_keys_nr_list_to_set.intersection(comp_scaffold_kmers_keys_list_to_set))
                            per = (num/len(comp_scaffold_kmers_keys_list_to_set))*100
                            if per > args.shared:
                                direction = "same-direction based on mapping of scaffold to scaffold kmer"
                                TypeVI = "\nsequence mapped to non-reverse-complemented scaffold"
                                same_type_list.append(TypeVI)
                            else:
                                direction = "Undetermined based on scaffold to scaffold mapping"
                        if (inc_scaffold_kmer_keys_list_to_set.isdisjoint(comp_scaffold_kmers_keys_list_to_set) == False and
                           inc_scaffold_kmer_keys_nr_list_to_set.isdisjoint(comp_scaffold_kmers_keys_list_to_set) == False):
                            direction = 'Undetermined based on mapping'
                            TypeV = "\nsequence mapped to reverse-complemented scaffold to scaffold kmer"
                            opposite_type_list.append(TypeV)
                            TypeVI = "\nsequence mapped to non-reverse-complemented scaffold to scaffold kmer"
                            same_type_list.append(TypeVI)
                            
                        if (len(same_type_list) != 0 and len(opposite_type_list) != 0):
                           direction = 'Undetermined based on scaffold to scaffold mapping'
                        if (len(same_type_list) == 0 and len(opposite_type_list) == 0):
                           direction = 'Undetermined based on scaffold to scaffold mapping'

            if (direction == "same-direction based on synteny" or direction == "opposite-direction based on synteny"):
                pos_diff = diff_start_end(scaffold_frag_start_pos, scaffold_frag_end_pos, frag_busco_index)
                overlap = overlap_check(pos_diff, frag_busco_index)
                if overlap == False:
                   pass
                else:
                    direction = direction + ' and overlapped' 

            if ((direction == "opposite-direction based on synteny") or 
               (direction == "opposite-direction based on mapping of flanking sequence") or
               (direction == "opposite-direction based on mapping of scaffold to scaffold kmer")):
                difference = self.inend - self.instart
                self.instart=len(inc_scaffold_str.strip('\n')) - self.inend + 1
                self.inend=self.instart + difference
                #Search common kmers before start positions of complete gene in reverse complemented 
                # as well as k-merized scaffold with fragmented BUSCO
                counter = 0                
                for startpos, sequence, endpos in comp_scaffold_start_str_end_sorted:
                    if inc_scaffold_kmers.get(sequence) == None:
                        continue    
                    position_list = inc_scaffold_kmers[sequence]
                    #print('start and end position of left flank of complete gene', (startpos, endpos))
                    if len(inc_scaffold_kmer_keys) != 0:
                        if inc_scaffold_kmer_keys[counter - 1] > position_list[0]:
                            continue
                        #print (sequence, position_list, (position_list[0]+int(args.kmer) - 1))    
                        left_flank_start_end = (int(startpos), int(endpos))
                        inc_scaffold_kmer_keys.append(position_list[0])
                        comp_scaffold_common_start_end.append(left_flank_start_end)
                        #print (position_list[0])
                        counter = counter + 1
                
                    if len(inc_scaffold_kmer_keys) == 0:
                        #print (sequence, position_list, (position_list[0]+int(args.kmer) - 1))
                        left_flank_start_end = (int(startpos), int(endpos))
                        inc_scaffold_kmer_keys.append(position_list[0])
                        comp_scaffold_common_start_end.append(left_flank_start_end)
                        #print (position_list[0])
                        counter = counter + 1    

                #Search common kmers after end positions of complete gene in reverse complemented
                #as well as k-merized scaffold with fragmented BUSCO
                counter  = 0
                for startpos, sequence, endpos in comp_scaffold_start2_str_end2:
                    if inc_scaffold_kmers.get(sequence) == None:
                        continue
                    position_list = inc_scaffold_kmers[sequence]
                    #print('start and end position of right flank of complete gene', (startpos, endpos))
                    if len(inc_scaffold_kmer_keys2) != 0:
                        if inc_scaffold_kmer_keys2[counter - 1] > position_list[-1]:
                            continue
                        #print (sequence, position_list, (position_list[0]+int(args.kmer) - 1))    
                        right_flank_start_end = (int(startpos), int(endpos))
                        inc_scaffold_kmer_keys2.append(position_list[-1])
                        comp_scaffold_common_start2_end2.append(right_flank_start_end)
                        #print (position_list[-1])
                        counter = counter + 1
                    if len(inc_scaffold_kmer_keys2) == 0:
                        #print (sequence, position_list, (position_list[0]+int(args.kmer) - 1))
                        right_flank_start_end = (int(startpos), int(endpos))
                        inc_scaffold_kmer_keys2.append(position_list[-1])
                        comp_scaffold_common_start2_end2.append(right_flank_start_end)
                        #print (position_list[-1])
                        counter = counter + 1    
    

            if ((direction == "same-direction based on synteny") or 
               (direction == "same-direction based on mapping of flanking sequence") or
               (direction == "same-direction based on mapping of scaffold to scaffold kmer")):
                #Search common kmers before start positions of complete gene in non-reverse complemented 
                #as well as k-merized scaffold with fragmented BUSCO
                counter = 0         
                for startpos, sequence, endpos in comp_scaffold_start_str_end_sorted:
                    if inc_scaffold_kmers_nr.get(sequence) == None:
                        continue
                    position_list = inc_scaffold_kmers_nr[sequence]
                    #print('start and end position of left flank of complete gene', (startpos, endpos)) 
                    if len(inc_scaffold_kmer_keys) != 0:
                        if inc_scaffold_kmer_keys[counter - 1] > position_list[0]:
                            continue
                        #print (sequence, position_list, (position_list[0]+int(args.kmer) - 1))    
                        left_flank_start_end = (int(startpos), int(endpos))
                        inc_scaffold_kmer_keys.append(position_list[0])
                        comp_scaffold_common_start_end.append(left_flank_start_end)
                        #print (position_list[0])
                        counter = counter + 1
                    if len(inc_scaffold_kmer_keys) == 0:
                        #print (sequence, position_list, (position_list[0]+int(args.kmer) - 1))
                        left_flank_start_end = (int(startpos), int(endpos))
                        inc_scaffold_kmer_keys.append(position_list[0])
                        comp_scaffold_common_start_end.append(left_flank_start_end)
                        #print (position_list[0])
                        counter = counter + 1    

                #Search common kmers after start positions of complete gene in non-reverse complemented 
                #as well as k-merized scaffold with fragmented BUSCO
                counter = 0           
                for startpos, sequence, endpos in comp_scaffold_start2_str_end2:
                    if inc_scaffold_kmers_nr.get(sequence) == None:
                        continue
                    position_list = inc_scaffold_kmers_nr[sequence]
                    #print(position_list)
                    #print('start and end position of right flank of complete gene', (startpos, endpos)) 
                    if len(inc_scaffold_kmer_keys2) != 0:
                        if inc_scaffold_kmer_keys2[counter - 1] > position_list[-1]:
                            continue
                        #print (sequence, position_list, (position_list[0]+int(args.kmer) - 1))    
                        right_flank_start_end = (int(startpos), int(endpos))
                        inc_scaffold_kmer_keys2.append(position_list[-1])
                        comp_scaffold_common_start2_end2.append(right_flank_start_end)
                        #print (position_list[-1])
                        counter = counter + 1
                    if len(inc_scaffold_kmer_keys2) == 0:
                        #print (sequence, position_list, (position_list[0]+int(args.kmer) - 1))
                        right_flank_start_end = (int(startpos), int(endpos))
                        inc_scaffold_kmer_keys2.append(position_list[-1])
                        comp_scaffold_common_start2_end2.append(right_flank_start_end)
                        #print (position_list[-1])
                        counter = counter + 1    

            length_of_inc_keys = len(inc_scaffold_kmer_keys)
          
            length_of_inc_keys2 = len(inc_scaffold_kmer_keys2)

            comp_start_gene_end_for_slice=list()    

            if length_of_inc_keys == 0 and length_of_inc_keys2 == 0:
                pass

            elif length_of_inc_keys == 0 and length_of_inc_keys2 != 0:
                begin, finish = comp_scaffold_common_start2_end2[-1]
                self.end = finish          
                self.inend=inc_scaffold_kmer_keys2[-1]+(int(args.kmer) - 1)
                  
            elif length_of_inc_keys != 0 and length_of_inc_keys2 == 0:
                begin, finish = comp_scaffold_common_start_end[0]
                self.start=begin
                self.instart=inc_scaffold_kmer_keys[0]
          
   
            elif length_of_inc_keys != 0 and length_of_inc_keys2 != 0:
                begin, finish = comp_scaffold_common_start_end[0]
                self.start=begin
                self.instart=inc_scaffold_kmer_keys[0]
                begin, finish = comp_scaffold_common_start2_end2[-1]
                self.end = finish 
                self.inend=inc_scaffold_kmer_keys2[-1]+(int(args.kmer) - 1)
            comp_start_gene_end_for_slice.append(self.busco)
            comp_start_gene_end_for_slice.append(self.cscaffold)
            comp_start_gene_end_for_slice.append(self.start)
            comp_start_gene_end_for_slice.append(self.end)
            comp_start_gene_end_for_slice.append(self.inscaffold)
            comp_start_gene_end_for_slice.append(self.instart)
            comp_start_gene_end_for_slice.append(self.inend)
            comp_start_gene_end_for_slice.append(direction)
            comp_gene_scaffold_file_handle.close()
            inc_gene_file_handle.close()
            return comp_start_gene_end_for_slice


in_f = args.input.rstrip('/')
f_h = open(in_f, 'r')
f = open(args.single.rstrip('/'), 'w')
read_fasta(f_h, f) 
f.close() 

f_b = open(args.single.rstrip('/'), 'r')
dic = make_dictionary(f_b)

'''Parse the data in the input csv file'''
rev_comp_busco_table_dict = parse_rev_busco_table(args.revcompbt)
output_after_processing_csv = list()
input_data_from_csv = []
path = args.data.rstrip('/')
data_file_handler = open(path)
reader = csv.reader(data_file_handler)
header = next(reader)
scaffold_direction = {}
for row in reader:
    BUSCO_name=str(row[0])
    Scaffold_com=str(row[1])
    Start_com=int(row[2])
    End_com=int(row[3])
    Scaffold_inc=str(row[4])
    Start_inc=int(row[5])
    End_inc=int(row[6])
    input_data_from_csv.append([BUSCO_name, Scaffold_com, Start_com, End_com, Scaffold_inc, Start_inc, End_inc])
            
for  i in range(len(input_data_from_csv)):
    busco_names = input_data_from_csv[i][0]
    comp_scaffold = input_data_from_csv[i][1]
    comp_start = input_data_from_csv[i][2]
    comp_end = input_data_from_csv[i][3]
    inc_scaffold = input_data_from_csv[i][4]
    inc_start = input_data_from_csv[i][5]
    inc_end = input_data_from_csv[i][6]
    if rev_comp_busco_table_dict[busco_names] == 'Complete':
        print("Fragmented ",busco_names, " is complete when scaffold is reverse complemented so it is not being processed")
        continue
    BUSCO_info = completeBUSCO(busco_names, comp_scaffold, comp_start, comp_end, inc_scaffold, inc_start, inc_end)
    '''Apply class CompleteBUSCO on the input data with BUSCO info in the csv file'''
    list_of_BUSCO_info = completeBUSCO.gene(BUSCO_info)
    if ((list_of_BUSCO_info[-1] == "same-direction based on synteny and overlapped") or 
       (list_of_BUSCO_info[-1] == "opposite-direction based on synteny and overlapped")):
       print("we are not processing" + "---->" + list_of_BUSCO_info[0], 'is', list_of_BUSCO_info[-1])
       continue
    if (list_of_BUSCO_info[-1] == "Undetermined based on scaffold to scaffold mapping"):
       print("we are not processing" + "---->" + list_of_BUSCO_info[0], 'is', list_of_BUSCO_info[-1])
       continue
    if (list_of_BUSCO_info[-1] == "unresolvable due to complex synteny" or list_of_BUSCO_info[-1] == "unresolvable"):
       print("we are not processing" + "---->" + list_of_BUSCO_info[0], 'is', list_of_BUSCO_info[-1]) 
    output_after_processing_csv.append(list_of_BUSCO_info)
print (output_after_processing_csv)
    #print(list_of_BUSCO_info)

comp_scaffold_list = []
comp_scaffold_comp_dictionary = {}
comp_scaffold_diction = {}
difference_diction = {}
overlap_check_diction = {}
status_before_after = {}
inc_scaffold_comp_dictionary = {}
inc_scaffold_diction = {}
dic_new = {}
dic_new_comp_old_start = {}
dic_new_comp_old_end = {}
dic_new_inc_old_start = {}
dic_new_inc_old_end = {}

for data in output_after_processing_csv:
    name_of_busco = data[0]
    name_of_comp_scaffold = data[1]
    new_comp_start = data[2]
    new_comp_end = data[3]
    name_of_inc_scaffold = data[4]
    new_inc_start = data[5]
    new_inc_end = data[6]
    rev_comp_status = data[7].split(' ')[0]
    if  name_of_inc_scaffold in scaffold_direction:
        if rev_comp_status in scaffold_direction[name_of_inc_scaffold]:
           pass 
        if rev_comp_status not in scaffold_direction[name_of_inc_scaffold]:
           scaffold_direction[name_of_inc_scaffold].append(rev_comp_status)
    if  name_of_comp_scaffold not in scaffold_direction:
        scaffold_direction[name_of_inc_scaffold] = [rev_comp_status]


for data in output_after_processing_csv:
    name_of_busco = data[0]
    name_of_comp_scaffold = data[1]
    new_comp_start = data[2]
    new_comp_end = data[3]
    name_of_inc_scaffold = data[4]
    new_inc_start = data[5]
    new_inc_end = data[6]
    rev_comp_status = data[7].split(' ')[0]
    if len(scaffold_direction[name_of_inc_scaffold]) > 1:
        print("The Fragmented buscos in the same scaffold have different directions in other assemblies")
        continue 
    filename_comp = name_of_comp_scaffold + '.fa'
    filename_inc = name_of_inc_scaffold + '.fa'
    filename_rev_comp = name_of_inc_scaffold +'Yes.fa'

    #List to load sequence
    seq = []
    seq_2 = []
    #List to load 'Ns'
    gap = []
    gap_2 = []
    #List to load tuple of start position of contig, contig sequence, and end position of the contig
    start_end_status = []
    start_end_status_2 = []
    #List to load contigs sequences and gaps the scaffolds
    contigs = []
    contigs_2 = [] 

    start_position=0
    end_position=0
    start_position_2=0
    end_position_2=0
    
    in_file_4 = args.complete.rstrip('/') + '/' + filename_comp
    fh_4 = open(in_file_4, 'r')
    comp_scaffold_forward_reference_str = sequence_filter(fh_4)
    comp_scaffold_forward_reference_str = comp_scaffold_forward_reference_str.strip('\n')


    in_file_5 = args.fragmented.rstrip('/') + '/' + filename_inc
    fh_5 = open(in_file_5, 'r')
    inc_scaffold_forward_reference = sequence_filter(fh_5)
    inc_scaffold_forward_reference_str = inc_scaffold_forward_reference.strip('\n')
    inc_scaffold_reverse_comp_query = reverse_complement(inc_scaffold_forward_reference_str)

    in_file_6 = args.revcomp.rstrip('/') + '/' + filename_rev_comp
    fh_6 = open(in_file_6, 'r')

    #comp_scaffold_forward_or_reverse_query_str = '' 

    inc_scaffold_forward_or_reverse_query_str = '' 

    if rev_comp_status == "opposite-direction":
        inc_scaffold_forward_or_reverse_query_str += inc_scaffold_reverse_comp_query.strip('\n')

        #comp_scaffold_forward_or_reverse_query = sequence_filter(fh_6)
        #comp_scaffold_forward_or_reverse_query_str += comp_scaffold_forward_or_reverse_query.strip('\n')

    if rev_comp_status == "same-direction":
        inc_scaffold_forward_or_reverse_query_str += inc_scaffold_forward_reference_str.strip('\n')

        #comp_scaffold_forward_or_reverse_query = sequence_filter(fh_4)
        #comp_scaffold_forward_or_reverse_query_str += comp_scaffold_forward_or_reverse_query.strip('\n')
        #comp_scaffold_forward_or_reverse_query_str = comp_scaffold_forward_or_reverse_query_str.strip('\n')
        #comp_scaffold_forward_or_reverse_query_str = comp_scaffold_forward_or_reverse_query_str.upper()
        #inc_scaffold_forward_reference_str = inc_scaffold_forward_reference_str.strip('\n')
        #inc_scaffold_forward_reference_str = inc_scaffold_forward_reference_str.upper()
        #print (len(comp_scaffold_forward_or_reverse_query_str))

    if rev_comp_status == "Undetermined" or rev_comp_status == "unresolvable":
        continue

    if args.contig:
        if inc_scaffold_forward_or_reverse_query_str[0] !='N':
            for seqs in re.findall(r'([ATGC]+)', inc_scaffold_forward_or_reverse_query_str.upper()):
                seq.append(seqs)
            for gaps in re.findall(r'([N]+)', inc_scaffold_forward_or_reverse_query_str.upper()):
                 gap.append(gaps)
            s_len = len(seq)
            g_len = len(gap)
            if s_len == g_len:
                for i in range(int(s_len)):
                    contigs.append(seq[i])
                    contigs.append(gap[i])
            if s_len > g_len:
                for i in range(int(s_len)):
                    if i < (int(s_len) - 1): 
                       contigs.append(seq[i])
                       contigs.append(gap[i])
                    if i == (int(s_len) - 1):
                       contigs.append(seq[i])

        if inc_scaffold_forward_or_reverse_query_str[0]=='N':
            for seqs in re.findall(r'([ATGC]+)', inc_scaffold_forward_or_reverse_query_str.upper()):
                seq.append(seqs)
            for gaps in re.findall(r'([N]+)', inc_scaffold_forward_or_reverse_query_str.upper()):
                gap.append(gaps)
            s_len = len(seq)
            g_len = len(gap)
            if s_len == g_len:
               for i in range(int(g_len)):
                   contigs.append(gap[i])
                   contigs.append(seq[i])
            if g_len > s_len:
               for i in range(int(g_len)):
                    if i < (int(g_len) - 1):
                       contigs.append(gap[i])
                       contigs.append(seq[i])
                    if i == (int(g_len) - 1):
                       contigs.append(gap[i])                              
              
        for element in contigs:
            start_position=end_position+1
            end_position=int(len(element)) + end_position
            t = (int(start_position), element, int(end_position))
            start_end_status.append(t)
              

        if comp_scaffold_forward_reference_str[0]!='N':
            for seqs in re.findall(r'([ATGC]+)', comp_scaffold_forward_reference_str.upper()):
               seq_2.append(seqs)
            for gaps in re.findall(r'([N]+)', comp_scaffold_forward_reference_str.upper()):
                gap_2.append(gaps)
            s_len = len(seq_2)
            g_len = len(gap_2)
            if s_len == g_len:
               for i in range(int(s_len)):
                   contigs_2.append(seq_2[i])
                   contigs_2.append(gap_2[i])
            if s_len > g_len:
                for i in range(int(s_len)):
                    if i < (int(s_len) - 1): 
                       contigs_2.append(seq_2[i])
                       contigs_2.append(gap_2[i])
                    if i == (int(s_len) - 1):
                       contigs_2.append(seq_2[i])           

        elif comp_scaffold_forward_reference_str[0]=='N':
            for seqs in re.findall(r'([ATGC]+)', comp_scaffold_forward_reference_str.upper()):
                seq_2.append(seqs)
            for gaps in re.findall(r'([N]+)', comp_scaffold_forward_reference_str.upper()):
                gap_2.append(gaps)
            s_len = len(seq_2)
            g_len = len(gap_2)
            if s_len == g_len:
               for i in range(int(g_len)):
                   contigs_2.append(gap_2[i])
                   contigs_2.append(seq_2[i])
            if g_len > s_len:
                for i in range(int(g_len)):
                    if i < (int(g_len) - 1):
                      contigs_2.append(gap_2[i])
                      contigs_2.append(seq_2[i])
                    if i == (int(g_len) - 1):
                      contigs_2.append(gap_2[i])

        for element in contigs_2:
            start_position_2=end_position_2+1
            end_position_2=int(len(element))+end_position_2
            t = (int(start_position_2), element, int(end_position_2))
            start_end_status_2.append(t)

        for start, element, end in start_end_status_2:
            if (int(start) < int(new_comp_start) < int(end)):
                new_comp_start = int(start)

            if int(new_comp_start) == int(start):
                new_comp_start = int(start)

            if int(new_comp_start) == int(end):
                new_comp_start = int(start)

        for start, element, end in start_end_status_2:             
            if int(start) < int(new_comp_end) < int(end):
                new_comp_end = int(end)
              
            if int(new_comp_end) == int(end):
                new_comp_end = int(end)

            if int(new_comp_end) == int(start):
                new_comp_end = int(end)

        for start, element, end in start_end_status:
            if (int(start) < int(new_inc_start) < int(end)):
                new_inc_start = int(start)

            if int(new_inc_start) == int(start):
                new_inc_start = int(start)

            if int(new_inc_start) == int(end):
                new_inc_start = int(start)

        for start, element, end in start_end_status:             
            if int(start) < int(new_inc_end) < int(end):
                new_inc_end = int(end)
              
            if int(new_inc_end) == int(end):
                new_inc_end = int(end)

            if int(new_inc_end) == int(start):
                new_inc_end = int(end)

    a = name_of_busco
    b = name_of_comp_scaffold 
    c = int(new_comp_start)
    d = int(new_comp_end)
    e = name_of_inc_scaffold
    f = int(new_inc_start)
    g = int(new_inc_end)
    h = rev_comp_status

    if f == 1 and e not in comp_scaffold_list:
        portion_of_comp = comp_scaffold_forward_reference_str[(c-1):(d)]
        second_portion_comp = comp_scaffold_forward_reference_str[(d):]
        portion_of_inc = inc_scaffold_forward_or_reverse_query_str[g:]
        replaced_inc = inc_scaffold_forward_or_reverse_query_str[(f-1):g]
        print(portion_of_comp[0:19],'............',portion_of_comp[-19:],
        '|||',portion_of_inc[0:19], a)
        print (replaced_inc[0:19],'............',replaced_inc[-19:],
        '|||',second_portion_comp[0:19], a)
        dic[e]= portion_of_comp + portion_of_inc
        dic_new[e] = dic[e]
        dic_new_comp_old_start[e] = c
        dic_new_comp_old_end[e] = d
        dic_new_inc_old_start[e] = f
        dic_new_inc_old_end[e] = g
        comp_scaffold_list.append(b)
        comp_scaffold_list.append(e)
        difference = (d-c+1)-(g-f+1)
        difference_diction[e] = difference
        overlap_check_diction[e] = g
        status_before_after[e] = h
               
    elif f!=1 and e not in comp_scaffold_list:
        portion_of_inc_1 = inc_scaffold_forward_or_reverse_query_str[0:(f-1)]
        first_portion_comp = comp_scaffold_forward_reference_str[0:(c-1)]
        second_portion_comp = comp_scaffold_forward_reference_str[d:]
        portion_of_comp = comp_scaffold_forward_reference_str[(c-1):(d)]
        replaced_inc = inc_scaffold_forward_or_reverse_query_str[(f-1):(g)]
        portion_of_inc_2 = inc_scaffold_forward_or_reverse_query_str[(g):]
        print(portion_of_inc_1[-19:],'|||',portion_of_comp[0:19],'............',
        portion_of_comp[-19:],'|||',portion_of_inc_2[0:19], a)
        print (first_portion_comp[-19:],'|||',replaced_inc[0:19],'............',replaced_inc[-19:],
        '|||',second_portion_comp[0:19], a)
        dic[e] = portion_of_inc_1 + portion_of_comp + portion_of_inc_2
        dic_new[e] = dic[e]
        dic_new_comp_old_start[e] = c
        dic_new_comp_old_end[e] = d
        dic_new_inc_old_start[e] = f
        dic_new_inc_old_end[e] = g
        comp_scaffold_list.append(b)
        comp_scaffold_list.append(e)
        difference = (d-c+1)-(g-f+1)
        difference_diction[e] = difference
        overlap_check_diction[e] = g
    
    elif f != 1 and e in comp_scaffold_list and f > overlap_check_diction[e]:
                f = f + int(difference_diction[e])
                g = g + int(difference_diction[e])
                string = dic_new[e]
                dic[e] = string[:(f-1)]+comp_scaffold_forward_reference_str[(c-1):d]+string[g:]
                portion_of_inc_1 = string[0:(f-1)]
                first_portion_comp = comp_scaffold_forward_reference_str[0:(c-1)]
                second_portion_comp = comp_scaffold_forward_reference_str[d:]
                portion_of_comp = comp_scaffold_forward_reference_str[(c-1):(d)]
                replaced_inc = string[(f-1):(g)]
                portion_of_inc_2 = string[(g):]
                print(portion_of_inc_1[-19:],'|||',portion_of_comp[0:19],'............',
                '|||',second_portion_comp[0:19], a)
                print (first_portion_comp[-19:],'|||',replaced_inc[0:19],'............',replaced_inc[-19:],
                '|||',second_portion_comp[0:19], a)
                difference = (d-c+1)-(g-f+1)
                difference_diction[e] = difference
                overlap_check_diction[e] = g   

    elif f==1 and e in comp_scaffold_list and f > overlap_check_diction[e]:
                string = dic_new[e]
                dic[e] = comp_scaffold_forward_reference_str[(c-1):d]+string[g:]
                difference = (d-c+1)-(g-f+1)
                difference_diction[e] = difference
                overlap_check_diction[e] = g
          
    fh_4.close()
    fh_5.close()
    fh_6.close() 
f_b.close()    
f_b = open(args.single.rstrip('/'), 'r')
f_2 = open(args.output.rstrip('/'), 'w')
for number, line in enumerate(f_b):
    line = line.strip('\n')
    line = line.strip()
    if number % 2 == 0:
        f_2.write(line+'\n')
        f_2.write(dic[line]+'\n')    
f_b.close()
f_2.close()
    
    
    



    
